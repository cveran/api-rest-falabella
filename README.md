# API REST FALABELLA

Estimados:

1.- En la rama master pueden encontrar los archivos https://gitlab.com/cveran/api-rest-falabella/-/tree/master/ApiFalabella

2.-  Desde el siguiente link puede clonar el repositorio https://gitlab.com/cveran/api-rest-falabella.git

3.- URL directa de consumo en GCP http://springboot-gcp-331423.rj.r.appspot.com/, en la siguiente ruta pueden encontrar el postman collection para relaizar pruebas https://gitlab.com/cveran/api-rest-falabella/-/blob/master_1/API%20FALABELLA%201.0%20DEV.postman_collection.json. 

4.- Ir a la url http://127.0.0.1:8090/h2-ui si se trabaja en local o la url de GCP http://springboot-gcp-331423.rj.r.appspot.com/h2-ui, es una BD H2, deben ingresar en JDBC URL: jdbc:h2:mem:db_falabella, User Name:falabella y en Password:falabella, luego darle a conectar, a continuacion tendran acceso de la BD.

5.- Para ver los fuentes y correr el proyecto les recomiendo bajar un ide, https://spring.io/tools , ya viene con un servidor integrado (tomcat), cuando importen el proyecto solo le dan click derecho al proyecto y luego Run AS > Spring boot app, se les va a levantar el APP en el puerto 8090.


Resumen: Se construyó una aplicación rest básica exponiendo los métodos "CRUD" el diseño es bastante simple ya que de un controller, pasamos directo a consumir a un repositorio, en un futuro se buscará desacoplar las clases con Interfaces para mantener un código más limpio, queda pendiente las pruebas unitarias, dado el tiempo no me fue posible realizarlas. 


Muchas gracias por la oportunidad!!!





